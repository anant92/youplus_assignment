<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->library('javascript');
    }

	public function index()
	{
		$this->load->view('shared/header');
		$this->load->view('customer/addRequest');
	}

	public function AddRequest(){
		$customerId = $this->input->post('CustomerId');
		$this->load->model('RequestModel');
		$this->RequestModel->createRequest($customerId);
		echo 'Request Added successfully';
	}
}
