<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->library('javascript');
    }
	public function index()
	{
		$this->load->model('RequestModel');
		$data['requests'] = $this->RequestModel->getIncompleteRequests();
		$this->load->view('shared/header');
		$this->load->view('admin/dashboard', $data);
	}
}
