<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Driver extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->library('javascript');
    }
	
	public function index($driverId)
	{
		$this->load->model('RequestModel');
		$waitingRequests = array();
		$ongoingRequests = array();
		$completedRequests = array();

		$waitingRequests = $this->RequestModel->getWaitingRequests();

		$requests = $this->RequestModel->getRequestsByDriverId($driverId);
		foreach ($requests as $req) {
			if($req->Status == 'Ongoing'){
				array_push($ongoingRequests,$req);
			}
			elseif($req->Status == 'Completed'){
				array_push($completedRequests,$req);
			}
		}
		$data['driverId'] = $driverId;
		$data['waitingRequests'] = $waitingRequests;
		$data['ongoingRequests'] = $ongoingRequests;
		$data['completedRequests'] = $completedRequests;
		$this->load->view('shared/header',$data);
		$this->load->view('driver/dashboard',$data);
	}

	public function SelectRide(){
		$driverId = $this->input->post('driverId');
		$requestId = $this->input->post('requestId');
		$this->load->model('RequestModel');

		$isDriverBusy = $this->RequestModel->checkDriverBusy($driverId);
		$request = $this->RequestModel->getRequestById($requestId);

		if ($request[0]->Status != 'Waiting'){
			$data['status'] = '201';
			$data['message'] = 'Sorry, request already accepted by another driver!!';
		}
		elseif ($isDriverBusy){
			$data['status'] = '201';
			$data['message'] = 'Cannot accept ride as driver has ongoing ride.';
		}else{
			$requests = $this->RequestModel->selectRequest($driverId, $requestId);
			$data['status'] = '200';
			$data['message'] = 'Ride accepted successfully.';
		}
		echo json_encode($data);
	}
}
