<div style='width:1100px;margin: 0 auto;margin-top:10px;'>
<button class="center-block btn btn-info" style="width: 200px;float:right;" onclick="location.reload();">Refresh</button>
<br><br>
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>Request Id</th>
      <th>Time Of Request</th>
      <th>Time Elapsed</th>
      <th>Current Status</th>
      <th>Driver</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($requests as $row){ ?>
      <tr>
        <td><?= $row->RequestId ?></td>
        <td><?= $row->RequestTime ?></td>
        <?php
          if($row->Status == 'Waiting'){
            $currentDateTime = new DateTime("now");
            $requestTime = new DateTime($row->RequestTime);
            $elapsedTime = date_diff($currentDateTime, $requestTime);
            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';
          }
          elseif ($row->Status == 'Ongoing') {
            $currentDateTime = new DateTime("now");
            $requestTime = new DateTime($row->PickUpTime);
            $elapsedTime = date_diff($currentDateTime, $requestTime);
            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';
          }

        ?>
        <td><?= $row->Status ?></td>
        <td><?= $row->DriverId == null ? 'None' : $row->DriverId ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table> 
</div>