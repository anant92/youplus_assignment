<head>
	<title>You Plus</title>
	<?= link_tag('public/css/bootstrap.min.css?version=1.0.0'); ?>
	<?= link_tag('public/css/font-awesome.min.css'); ?>

	<script src="<?= base_url('public/js/jquery-2.1.4.min.js') ?>" type="text/javascript" charset="utf-8"></script>
  	<script src="<?= base_url('public/js/bootstrap.min.js?version=1.0.0') ?> "></script>
  	<script src="<?= base_url('public/js/bootbox.min.js') ?> "></script>
  	<script src="<?= base_url('public/js/site.js') ?> "></script>
</head>