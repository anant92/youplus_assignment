<div style='width:1100px;margin: 0 auto;margin-top:10px;'>
<button class="center-block btn btn-info" style="width: 200px;float:right;" onclick="location.reload();">Refresh</button>
<br><br>

<ul class="nav nav-tabs">
  <li class="active"><a href="#waiting" data-toggle="tab">Waiting</a></li>
  <li><a href="#ongoing" data-toggle="tab">Ongoing</a></li>
  <li><a href="#completed" data-toggle="tab">Completed</a></li>
</ul>

<div id="" class="tab-content">
  <div class="tab-pane fade active in" id="waiting">
  		<table class="table table-striped table-hover ">
		  <thead>
		    <tr>
		      <th>Request Id</th>
		      <th>Customer Id</th>
		      <th>Time Elapsed</th>
		      <th>Action</th>
		    </tr>
		  </thead>
		  <tbody>
		    <?php foreach ($waitingRequests as $row){ ?>
		      <tr>
		        <td><?= $row->RequestId ?></td>
		        <td><?= $row->CustomerId ?></td>
		        <?php
		            $currentDateTime = new DateTime("now");
		            $requestTime = new DateTime($row->RequestTime);
		            $elapsedTime = date_diff($currentDateTime, $requestTime);
		            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';
		        ?>
		        <td><a class="center-block btn btn-info" style="" onclick="SelectRide(<?=$driverId?>,<?=$row->RequestId?>)">Select</a></td>
		      </tr>
		    <?php } ?>
		  </tbody>
		</table> 
  </div>
  <div class="tab-pane fade" id="ongoing">
  		<table class="table table-striped table-hover ">
		  <thead>
		    <tr>
		      <th>Request Id</th>
		      <th>Customer Id</th>
		      <th>Request</th>
		      <th>Picked Up</th>
		    </tr>
		  </thead>
		  <tbody>
		    <?php foreach ($ongoingRequests as $row){ ?>
		      <tr>
		        <td><?= $row->RequestId ?></td>
		        <td><?= $row->CustomerId ?></td>
		        <?php
		            $currentDateTime = new DateTime("now");
		            $requestTime = new DateTime($row->RequestTime);
		            $elapsedTime = date_diff($currentDateTime, $requestTime);
		            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';

		            $pickUpTime = new DateTime($row->PickUpTime);
		            $elapsedTime = date_diff($currentDateTime, $pickUpTime);
		            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';
		        ?>
		      </tr>
		    <?php } ?>
		  </tbody>
		</table> 
  </div>
  <div class="tab-pane fade" id="completed">
  		<table class="table table-striped table-hover ">
		  <thead>
		    <tr>
		      <th>Request Id</th>
		      <th>Customer Id</th>
		      <th>Request</th>
		      <th>Picked Up</th>
		      <th>Completed</th>
		    </tr>
		  </thead>
		  <tbody>
		    <?php foreach ($completedRequests as $row){ ?>
		      <tr>
		        <td><?= $row->RequestId ?></td>
		        <td><?= $row->CustomerId ?></td>
		        <?php
		            $currentDateTime = new DateTime("now");
		            $requestTime = new DateTime($row->RequestTime);
		            $elapsedTime = date_diff($currentDateTime, $requestTime);
		            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';

		            $pickUpTime = new DateTime($row->PickUpTime);
		            $elapsedTime = date_diff($currentDateTime, $pickUpTime);
		            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';

		            $completeTime = new DateTime($row->CompleteTime);
		            $elapsedTime = date_diff($currentDateTime, $completeTime);
		            echo '<td>'.$elapsedTime->format('%i min %s sec').'</td>';
		        ?>

		      </tr>
		    <?php } ?>
		  </tbody>
		</table> 
  </div>
</div>

<script>
	function SelectRide(driverId, requestId) {
	    $.ajax({
	      url: "<?= base_url('/Driver/SelectRide'); ?>",
	      type: 'post',
	      dataType: 'json',
	      data: {driverId: driverId, requestId: requestId},
	      success: function(data) {
	      		if(data.status == '201'){
	      			bootbox.alert(data.message);
	      		}
	      		else{
	      			bootbox.alert('Request accepted successfully.', function(){
	      				location.reload();
	      			});

	      		}
	      }
	    });
	} 
</script>