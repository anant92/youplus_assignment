<?php
class RequestModel extends CI_Model {
    function __construct()
    {
          parent::__construct();
          $this->load->database(); 
    }

    function RequestModel()
    {
        // Call the Model constructor
        parent::Model();
    }

    function getRequestById($requestId){
        $query = $this->db->where('RequestId', $requestId)->get('request');
        return $query->result();
    }
     
    function getRequests()
    {
        //Query the data table for every record and row
        $query = $this->db->get('request');
        //print 'infy'.$query->num_rows(); 
        if ($query->num_rows() > 0)
        {
            return $query->row();
            //show_error('Database is empty!');
        }else{
            return $query->result();
        }
    }
 
    function createRequest($customerId){
        $data = array(
           'CustomerId' => $customerId ,
           'RequestTime' => date('Y/m/d H:i:s', time()),
           'Status' => "Waiting"
        );
        $this->db->insert('request', $data); 
        return $this->db->error();
    }

    function getIncompleteRequests(){
        $query = $this->db->where('Status !=', 'Completed')->get('request');
        return $query->result();
    }

    function getRequestsByDriverId($driverId){
        $query = $this->db->where('DriverId', $driverId)->get('request');
        return $query->result();
    }

    function getWaitingRequests(){
        $query = $this->db->where('Status', 'Waiting')->get('request');
        return $query->result();
    }

    function selectRequest($driverId, $requestId){
        $this->db->set('DriverId', $driverId);
        $this->db->set('Status', 'Ongoing');
        $this->db->set('PickUpTime', date('Y/m/d H:i:s', time()));
        $this->db->where('RequestId', $requestId);
        $this->db->update('request');
    }

    function checkDriverBusy($driverId){
        $query = $this->db->where('DriverId', $driverId)->where('Status', 'Ongoing')->get('request');
        if ($query->num_rows() > 0)
        {
            return true;
        }
        return false;
    }
}

?>
